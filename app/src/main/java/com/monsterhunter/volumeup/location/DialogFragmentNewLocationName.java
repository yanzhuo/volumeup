package com.monsterhunter.volumeup.location;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.monsterhunter.volumeup.PreferenceHelper;
import com.monsterhunter.volumeup.R;

import java.util.List;

/**
 * Created by wyzbs on 16-01-25.
 */
public class DialogFragmentNewLocationName extends AppCompatDialogFragment {
    public interface NewLocationDialogListener {
        public void onDialogPositiveClick(String locationName);
        public void onDialogNegativeClick();
    }

    private NewLocationDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            mListener = (NewLocationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NewLocationDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_new_location_name, null);
        final EditText editText = (EditText)view.findViewById(R.id.et_new_location_name);
        final TextView textview = (TextView)view.findViewById(R.id.tv_error_text);

        builder.setView(view)
                .setPositiveButton(R.string.new_location_name_add_confirm, null)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //DialogFragmentNewLocationName.this.getDialog().cancel();
                        mListener.onDialogNegativeClick();
                    }
                });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textview.setText("");
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        String newLocationName = editText.getText().toString();
                        if (newLocationName.isEmpty()) {
                            textview.setText("Name cannot be empty");
                        } else if (!IsLocationNameValid(newLocationName)) {
                            textview.setText("Name already exists");
                        } else {
                            mListener.onDialogPositiveClick(newLocationName);
                            alertDialog.dismiss();
                        }
                    }
                });
            }
        });
        return alertDialog;
    }

    private boolean IsLocationNameValid(String newName) {
        PreferenceHelper preferenceHelper = new PreferenceHelper(getContext());
        List<LocationConfig> locationConfigs = preferenceHelper.GetAllLocationConfigs();
        for(LocationConfig l : locationConfigs) {
            if (l.uniqueName.equals(newName)) {
                return false;
            }
        }

        return true;
    }

}
