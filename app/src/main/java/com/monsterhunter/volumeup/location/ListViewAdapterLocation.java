package com.monsterhunter.volumeup.location;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.monsterhunter.volumeup.PreferenceHelper;
import com.monsterhunter.volumeup.R;
import com.monsterhunter.volumeup.choicemode.ChoiceCapableAdapter;
import com.monsterhunter.volumeup.choicemode.ChoiceCapableViewHolder;
import com.monsterhunter.volumeup.choicemode.IChoiceMode;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyzbs on 16-01-25.
 */
public class ListViewAdapterLocation extends ChoiceCapableAdapter<ListViewAdapterLocation.ViewHolder> {
    private List<LocationConfig> m_items;
    private Context m_context;
    private Activity m_activity;
    private IOnDataChangedListener mOnDataChangedListener;

    public ListViewAdapterLocation(List<LocationConfig> locationConfigs, Context context,
                                   Activity activity, IOnDataChangedListener dataChangedListener,
                                   IChoiceMode choiceMode) {
        super(choiceMode);
        m_items = locationConfigs;
        m_context = context;
        m_activity = activity;
        mOnDataChangedListener = dataChangedListener;
    }

    @Override
    public void deleteCheckedItems() {
        final List<Integer> posToDel= choiceMode.getCheckedPositions();
        Collections.sort(posToDel, Collections.reverseOrder());
        PreferenceHelper preferenceHelper = new PreferenceHelper(m_activity);
        for (int position : posToDel) {
            preferenceHelper.RemoveLocationConfig(m_items.get(position).uniqueName);
            m_items.remove(position);
            notifyItemRemoved(position);
        }
        choiceMode.clearChecks();
        if (posToDel.size() > 0) {
            mOnDataChangedListener.onDataChanged();
        }
    }

    @Override
    public void clearCheckedState() {
        choiceMode.clearChecks();
    }

    public interface IOnDataChangedListener {
        void onDataChanged();
    }

    public static class ViewHolder extends ChoiceCapableViewHolder{
        public TextView tv_locationName;
        public TextView tv_locationMetaInfo;

        public ViewHolder(View v, IChoiceMode choiceMode) {
            super(v, choiceMode);
            tv_locationName = (TextView)v.findViewById(R.id.tv_location_name);
            tv_locationMetaInfo = (TextView)v.findViewById(R.id.tv_location_meta);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListViewAdapterLocation.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_location, parent, false);
        return new ViewHolder(v, this.choiceMode);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        LocationConfig locationConfig = m_items.get(position);
        viewHolder.itemView.setLongClickable(true);
        viewHolder.tv_locationName.setText(locationConfig.uniqueName);
        viewHolder.tv_locationMetaInfo.setText(
                locationConfig.address.isEmpty() ? locationConfig.coordinate.toString() : locationConfig.address);
        viewHolder.itemView.setSelected(choiceMode.isChecked(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return m_items.size();
    }

}
