package com.monsterhunter.volumeup.location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by wyzbs on 16-01-25.
 */
public class LocationConfig {
    public String address;
    public String uniqueName;
    public LatLng coordinate;

    public LocationConfig(String uniqueName) {
        this.uniqueName = uniqueName;
    }
}
