package com.monsterhunter.volumeup.location;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.monsterhunter.volumeup.DividerItemDecoration;
import com.monsterhunter.volumeup.PreferenceHelper;
import com.monsterhunter.volumeup.R;
import com.monsterhunter.volumeup.choicemode.ChoiceCapableAdapter;
import com.monsterhunter.volumeup.choicemode.IChoiceMode;
import com.monsterhunter.volumeup.choicemode.MultiChoiceMode;

import java.util.List;

/**
 * Created by wyzbs on 16-01-25.
 */
public class LocationSettingsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        DialogFragmentNewLocationName.NewLocationDialogListener,
        ListViewAdapterLocation.IOnDataChangedListener,
        IChoiceMode.IOnCheckModeChangedListener,
        android.support.v7.view.ActionMode.Callback {
    private RecyclerView mRecyclerView;
    private ChoiceCapableAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<LocationConfig> mAdapterData;
    private Place mNewPlace;
    private ActionMode mActionMode;
    private Toolbar mToolbar;
    private FloatingActionButton mFab;

    static final int PLACE_PICKER_REQUEST = 1;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_settings);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST);
        //mRecyclerView.addItemDecoration(itemDecoration);

        // specify an adapter
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        mAdapterData = preferenceHelper.GetAllLocationConfigs();
        IChoiceMode choiceModeHandler = new MultiChoiceMode();
        choiceModeHandler.setOnCheckModeChangedListener(this);
        mAdapter = new ListViewAdapterLocation(mAdapterData, this, this, this, choiceModeHandler);
        mRecyclerView.setAdapter(mAdapter);

        mFab = (FloatingActionButton) findViewById(R.id.fab);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(LocationSettingsActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment)).getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        this.mMap = map;
        setupMap();
    }

    private void setupMap() {
        if (mMap == null) {
            return;
        }
        mMap.clear();
        if (mAdapterData.size() !=  0) {
            LatLngBounds.Builder mapBoundBuilder = new LatLngBounds.Builder();
            for (LocationConfig location : mAdapterData) {
                mapBoundBuilder.include(location.coordinate);
                MarkerOptions options = new MarkerOptions()
                        .position(location.coordinate)
                        .title(location.uniqueName)
                        .snippet(location.address.isEmpty() ? location.coordinate.toString() : location.address);

                mMap.addMarker(options);
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mapBoundBuilder.build(), 700, 700, 100));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place == null) {
                    return;
                }
                mNewPlace = place;
                AppCompatDialogFragment dialog = new DialogFragmentNewLocationName();
                //dialog.show(getSupportFragmentManager(), "DialogFragmentNewLocationName");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(dialog, "DialogFragmentNewLocationName");
                transaction.commitAllowingStateLoss();
            }
        }
    }


    @Override
    public void onDialogPositiveClick(String locationName) {
        String toastMsg = String.format("Place: %s", mNewPlace.getName());
        Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        LocationConfig locationConfig = new LocationConfig(locationName);
        locationConfig.address = mNewPlace.getAddress().toString();
        locationConfig.coordinate = mNewPlace.getLatLng();
        preferenceHelper.SetLocationConfig(locationConfig);

        mAdapterData.add(locationConfig);
        mAdapter.notifyDataSetChanged();
        setupMap();
    }

    @Override
    public void onDialogNegativeClick() {

    }

    @Override
    public void onDataChanged() {
        setupMap();
    }

    @Override
    public void OnCheckModeChanged(boolean isOnCheckedMode) {
        if (isOnCheckedMode) {
            mActionMode = startSupportActionMode(this);
            mFab.animate().translationXBy(300);
        } else {
            if (mActionMode != null) {
                mActionMode.finish();
            }
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        getMenuInflater().inflate(R.menu.menu_context_mode, menu);
        mode.setTitle("DELETE");
        return(true);
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item_remove:
                mAdapter.deleteCheckedItems();
                mActionMode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mFab.animate().translationXBy(-300);
        mAdapter.clearCheckedState();
        mAdapter.notifyDataSetChanged();
    }

}
