package com.monsterhunter.volumeup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.monsterhunter.volumeup.choicemode.ChoiceCapableAdapter;
import com.monsterhunter.volumeup.choicemode.ChoiceCapableViewHolder;
import com.monsterhunter.volumeup.choicemode.IChoiceMode;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.util.Collections;
import java.util.List;

/**
 * Created by wyzbs on 16-01-18.
 */
public class ListViewAdapterWifiConfig extends ChoiceCapableAdapter<ListViewAdapterWifiConfig.ViewHolder> {
    private List<WifiListItem> m_wifiItems;
    private Context m_context;

    @Override
    public void deleteCheckedItems() {
        final List<Integer> posToDel= choiceMode.getCheckedPositions();
        Collections.sort(posToDel, Collections.reverseOrder());
        PreferenceHelper preferenceHelper = new PreferenceHelper(m_context);
        for (int position : posToDel) {
            preferenceHelper.RemoveWifiConfig(m_wifiItems.get(position).getId());
            m_wifiItems.remove(position);
            notifyItemRemoved(position);
        }
        choiceMode.clearChecks();
    }

    @Override
    public void clearCheckedState() {
        choiceMode.clearChecks();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends ChoiceCapableViewHolder implements ToggleButton.OnValueChangedListener{
        // each data item is just a string in this case
        public TextView m_wifiNames;
        public MultiStateToggleButton m_wifiState;
        public IMyViewHolderClicks m_listener;
        public ViewHolder(View v, IMyViewHolderClicks listener, IChoiceMode choiceMode) {
            super(v, choiceMode);
            m_listener = listener;
            m_wifiNames = (TextView)v.findViewById(R.id.wifi_name);
            m_wifiState = (MultiStateToggleButton)v.findViewById(R.id.mstb_wifi_state);
            m_wifiState.setOnValueChangedListener(this);
        }

        @Override
        public void onValueChanged(int value) {
            int pos = getAdapterPosition();
            m_listener.onValueChanged(pos, value);
        }

        public interface IMyViewHolderClicks {
            void onValueChanged(int itemPosition, int newValue);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListViewAdapterWifiConfig(List<WifiListItem> wifis, Context context, IChoiceMode choiceMode) {
        super(choiceMode);
        m_wifiItems = wifis;
        m_context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListViewAdapterWifiConfig.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_item_wifi, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v, new ViewHolder.IMyViewHolderClicks() {
            @Override
            public void onValueChanged(int itemPosition, int newValue) {
                m_wifiItems.get(itemPosition).setState(WifiListItem.State.fromInt(newValue));
                PreferenceHelper preferenceHelper = new PreferenceHelper(m_context);
                preferenceHelper.SetWifiConfig(m_wifiItems.get(itemPosition).getId(), newValue);
            }
        }, this.choiceMode);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WifiListItem wifiInfo = m_wifiItems.get(position);
        holder.m_wifiNames.setText(wifiInfo.getId());
        holder.m_wifiState.setValue(wifiInfo.getState().getValue());
        holder.itemView.setSelected(choiceMode.isChecked(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return m_wifiItems.size();
    }
}