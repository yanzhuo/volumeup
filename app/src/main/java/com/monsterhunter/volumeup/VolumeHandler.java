package com.monsterhunter.volumeup;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/**
 * Created by wyzbs on 15-12-16.
 */
public class VolumeHandler {
    private Context m_context;
    private AudioManager m_audioManager;
    public VolumeHandler(Context context) {
        m_context = context;
        m_audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }
    public void VolumeUp(double percentage) {
        if (m_audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE
                || m_audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {

            SetVolumeIfBig(percentage,AudioManager.STREAM_RING, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
            SetVolumeIfBig(percentage,AudioManager.STREAM_MUSIC, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
        }
    }

    public void VolumeDown(double percentage) {
        if (m_audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {

            SetVolumeIfSmall(percentage,AudioManager.STREAM_RING, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
            SetVolumeIfSmall(percentage,AudioManager.STREAM_MUSIC, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
        }
    }

    private int GetVolumeFromPercentage(double percentage, int stream) {
        return (int) (m_audioManager.getStreamMaxVolume(stream) * percentage);
    }

    private void SetVolumeIfBig(double percentage, int stream, int flags) {
        int volume = GetVolumeFromPercentage(percentage, stream);
        if (volume > m_audioManager.getStreamVolume(stream)) {
            m_audioManager.setStreamVolume(stream, volume, flags);
        }
    }
    private void SetVolumeIfSmall(double percentage, int stream, int flags) {
        int volume = GetVolumeFromPercentage(percentage, stream);
        if (volume < m_audioManager.getStreamVolume(stream)) {
            m_audioManager.setStreamVolume(stream, volume, flags);
        }
    }

}
