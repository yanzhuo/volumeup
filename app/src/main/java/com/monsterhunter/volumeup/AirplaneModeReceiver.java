package com.monsterhunter.volumeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;

/**
 * Created by wyzbs on 16-01-12.
 */
public class AirplaneModeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("android.intent.action.AIRPLANE_MODE")) {
            return;
        }
        // if is off
        if (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 0) {
            final Context contextFinal = context;
            //LocationRequestHandler locationRequest = new LocationRequestHandler(context);
            //locationRequest.requestUpdate();
            //locationRequest.requestUpdateNewInterval(10);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LocationRequestHandler locationRequest = new LocationRequestHandler(contextFinal);
                    //locationRequest.requestUpdate();
                    locationRequest.requestUpdate();
                }
            },30000);

        }

    }
}
