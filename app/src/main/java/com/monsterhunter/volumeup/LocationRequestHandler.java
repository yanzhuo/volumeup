package com.monsterhunter.volumeup;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by wyzbs on 16-01-09.
 */

public class LocationRequestHandler implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    enum Action {
        REQUEST(1), CANCEL(0);
        private int mval;

        Action(int val) {
            mval = val;
        }

        public int getValue() {
            return mval;
        }

        public static Action fromInt(int val) {
            for(Action a : Action.values()) {
                if(a.getValue() == val) {
                    return a;
                }
            }
            return CANCEL;
        }
    }

    private GoogleApiClient m_GoogleApiClient;
    private LocationRequest m_LocationRequest;
    private PendingIntent m_locationPendingIntent;
    private Context m_context;
    private static String TAG = LocationRequestHandler.class.getName();
    private int m_requestInterval = 1000 * 60;
    private Action m_action;

    public LocationRequestHandler(Context context) {
        m_context = context;
        m_requestInterval = convertMinToMiliSecond(1);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 23
                && ActivityCompat.checkSelfPermission(m_context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(m_context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (m_action == null) {
            m_action = Action.fromInt(new PreferenceHelper(m_context).getLocationRequestAction());
        }
        switch(m_action) {
            case REQUEST:
                Log.d(TAG, "requesting");
//                PreferenceHelper preferenceHelper = new PreferenceHelper(m_context);
//                preferenceHelper.setIsLocationRequestJustSent(true);
                LocationServices.FusedLocationApi.requestLocationUpdates(m_GoogleApiClient, m_LocationRequest, m_locationPendingIntent);
                break;
            case CANCEL:
                Log.d(TAG, "cancelling");
                LocationServices.FusedLocationApi.removeLocationUpdates(m_GoogleApiClient, m_locationPendingIntent);
                break;
        }

        onFinished();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private int convertMinToMiliSecond(int minute) {
        return minute * 1000 * 60;
    }

    private boolean isWifiConnected() {
        ConnectivityManager connManager = (ConnectivityManager) m_context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifiInfo.isConnected();
    }

    private boolean isAirplaneMode() {
        return Settings.Global.getInt(m_context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private boolean isEnabled() {
        PreferenceHelper preferenceHelper = new PreferenceHelper(m_context);
        return preferenceHelper.GetLocationUpdateEnabled();
    }

    public void requestUpdate() {
        Log.d(TAG, "Request location update.");
        Logger.Append("Requesting location update", true, m_context);
        if (isAirplaneMode() || isWifiConnected() || !isEnabled()) {
            onFinished();
            return;
        }
        initLocationRequest();
        initIntent();
        startGoogleApiClient();

        m_action = Action.REQUEST;
    }

    public void requestUpdateNewInterval(int requestIntervalInMinute) {
        Log.d(TAG, "Request location update. interval:" + requestIntervalInMinute);
        Logger.Append("Requesting location update with interval", true, m_context);
        m_requestInterval = convertMinToMiliSecond(requestIntervalInMinute);
        requestUpdate();
    }

    public void cancelRequest() {
        startGoogleApiClient();
        initIntent();
        // have to put into pref to avoid crash
        new PreferenceHelper(m_context).setLocationRequestAction(Action.CANCEL.getValue());
        m_action = Action.CANCEL;
    }

    private void startGoogleApiClient() {
        // google play client
        m_GoogleApiClient = new GoogleApiClient.Builder(m_context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        m_GoogleApiClient.connect();

        Log.d(TAG, "creating service");
    }

    private void initLocationRequest() {
        m_LocationRequest = new LocationRequest();
        m_LocationRequest.setInterval((int)(m_requestInterval * 1.1))
                .setFastestInterval((int)(m_requestInterval/1.5))
                .setMaxWaitTime((int)(m_requestInterval * 1.5))
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void initIntent() {
        Intent intent = new Intent(m_context, LocationIntentService.class);
        m_locationPendingIntent = PendingIntent.getService(m_context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void onFinished() {
        if (m_GoogleApiClient != null && m_GoogleApiClient.isConnected()) {
            m_GoogleApiClient.disconnect();
        }
    }
}
