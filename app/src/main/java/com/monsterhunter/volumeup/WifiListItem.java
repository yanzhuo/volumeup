package com.monsterhunter.volumeup;

/**
 * Created by wyzbs on 16-01-18.
 */
public class WifiListItem {

    public enum State{
        OFF(0), UP(1), DOWN(2);
        private int mval;

        State(int val) {
            mval = val;
        }

        public int getValue() {
            return mval;
        }

        public static State fromInt(int val) {
            for(State s : State.values()) {
                if(s.getValue() == val) {
                    return s;
                }
            }

            return null;
        }
    }


    public String m_id;
    private State m_state;

    public WifiListItem(String id, State state) {
        m_id = id;
        setState(state);
    }

    public String getId() {
        return m_id;
    }
    public void setId(String id) {
        m_id = id;
    }

    public State getState() {
        return m_state;
    }
    public void setState(State state) {
        m_state = state;
    }
}
