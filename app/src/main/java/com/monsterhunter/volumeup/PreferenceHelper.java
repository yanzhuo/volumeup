package com.monsterhunter.volumeup;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.monsterhunter.volumeup.location.LocationConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by wyzbs on 15-12-16.
 */
public class PreferenceHelper {
    private static final String PREFS_NAME = "DEFAULT_PREF";
    private static final String PREFS_LOCATION = "LOCATION_PREF";
    private static final String PREFS_WIFI = "WIFI_PREF";
    private static final String PREF_IS_ON = "IsOn";
    private static final String PREF_IS_LOCATION_UPDATE_ENABLED = "LocationUpdate";
    private static final String PREF_LAST_STATE = "LastOnOffState";
    private static final String PREF_MAX_VOL = "MaxVolume";
    private static final String PREF_IS_WIFI_JUST_OFF = "IsWifiJustOff";
    private static final String PREF_IS_LOC_REQUEST_JUST_SENT = "IsLocReqJustSent";
    private static final String PREF_LOC_UPDATE_INTERVAL = "LocUpdateInterval";
    private static final String PREF_LOC_REQUEST_ACTION = "LocReqAction";
    private static final String PREF_CUR_CONNECTED_WIFI_SSID = "CurConWifiSSID";

    private Context m_context;
    private SharedPreferences m_sharedPreference;

    public PreferenceHelper(Context context) {
        m_context = context;
        m_sharedPreference = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public int GetLastOnOffState() {
        return m_sharedPreference.getInt(PREF_LAST_STATE, -1);
    }

    public void SetLastOnOffState(int val) {
        SharedPreferences.Editor editor = m_sharedPreference.edit();
        editor.putInt(PREF_LAST_STATE, val);
        editor.apply();
    }

    public int GetMaxVolume() {
        return PreferenceManager.getDefaultSharedPreferences(m_context).getInt(m_context.getString(R.string.prefs_key_wifi_volume), 6) + 1;
    }

    boolean GetLocationUpdateEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(m_context).getBoolean(m_context.getString(R.string.prefs_key_location_switch),false);
        //return m_sharedPreference.getBoolean(PREF_IS_LOCATION_UPDATE_ENABLED, false);
    }

    public boolean GetIsWifiJustOff() {
        return m_sharedPreference.getBoolean(PREF_IS_WIFI_JUST_OFF, false);
    }

    public void SetIsWifiJustOff(boolean val){
        SharedPreferences.Editor editor = m_sharedPreference.edit();
        editor.putBoolean(PREF_IS_WIFI_JUST_OFF, val);
        editor.commit();
    }

    public void setIsLocationRequestJustSent(boolean val) {
        SharedPreferences.Editor editor = m_sharedPreference.edit();
        editor.putBoolean(PREF_IS_LOC_REQUEST_JUST_SENT, val);
        editor.commit();
    }

    public boolean getIsLocationRequestJustSent() {
        return m_sharedPreference.getBoolean(PREF_IS_LOC_REQUEST_JUST_SENT, false);
    }

    public List<WifiListItem> GetWifiItems() {
        List<WifiListItem> ret = new ArrayList<>();
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);

        Map<String,?> keys = preferences.getAll();
        if (keys !=  null) {
            for(Map.Entry<String,?> entry : keys.entrySet()){
                ret.add(new WifiListItem(entry.getKey(), WifiListItem.State.fromInt((Integer)(entry.getValue()))));
            }
        }

        return ret;
    }

    public void GetWifiItems(List<WifiListItem> store) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);

        Map<String,?> keys = preferences.getAll();
        if (keys !=  null) {
            for(Map.Entry<String,?> entry : keys.entrySet()){
                store.add(new WifiListItem(entry.getKey(), WifiListItem.State.fromInt((Integer)(entry.getValue()))));
            }
        }
    }

    public void AddWifi(String ssid) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);
        if (preferences.contains(ssid)) {
            return;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ssid, 0);
        editor.commit();
    }

    public void SetWifiConfig(String ssid, int value) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ssid, value);
        editor.commit();
    }

    public int GetWifiConfig(String ssid) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);
        return preferences.getInt(ssid, 0);
    }

    public void SetLocationConfig(LocationConfig obj) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(obj.uniqueName, json);
        editor.commit();
    }

    public LocationConfig GetLocationConfig(String locationUniqueName) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        String json = preferences.getString(locationUniqueName, "");
        LocationConfig obj = new Gson().fromJson(json, LocationConfig.class);
        return obj;
    }

    public List<LocationConfig> GetAllLocationConfigs() {
        List<LocationConfig> ret = new ArrayList<>();
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);

        Map<String,?> keys = preferences.getAll();
        Gson gson = new Gson();
        if (keys !=  null) {
            for(Map.Entry<String,?> entry : keys.entrySet()){
                String json = (String)entry.getValue();
                ret.add(gson.fromJson(json, LocationConfig.class));
            }
        }
        return ret;
    }

    public void GetAllLocationConfigs(List<LocationConfig> dataList) {
        dataList.clear();
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);

        Map<String,?> keys = preferences.getAll();
        Gson gson = new Gson();
        if (keys !=  null) {
            for(Map.Entry<String,?> entry : keys.entrySet()){
                String json = (String)entry.getValue();
                dataList.add(gson.fromJson(json, LocationConfig.class));
            }
        }
    }

    public void RemoveLocationConfig(String lctnName) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(lctnName);
        editor.commit();
    }

    public void RemoveWifiConfig(String ssid) {
        SharedPreferences preferences = m_context.getSharedPreferences(PREFS_WIFI, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(ssid);
        editor.commit();
    }

    public void setLocationUpdateInterval(int intervalInMinute) {
        SharedPreferences.Editor editor = m_sharedPreference.edit();
        editor.putInt(PREF_LOC_UPDATE_INTERVAL, intervalInMinute);
        editor.commit();
    }

    public int getLocationUpdateInterval() {
        return m_sharedPreference.getInt(PREF_LOC_UPDATE_INTERVAL, 2);
    }

    public int getLocationRequestAction() {
        return m_sharedPreference.getInt(PREF_LOC_UPDATE_INTERVAL, 0);
    }

    public void setLocationRequestAction(int action) {
        m_sharedPreference.edit().putInt(PREF_LOC_REQUEST_ACTION, action).commit();
    }

    public boolean getVolumeDownOnWifiDisconnect() {
        return PreferenceManager.getDefaultSharedPreferences(m_context).getBoolean(m_context.getString(R.string.Prefs_key_volume_down_wifi_disconnect_switch),false);
    }
}
