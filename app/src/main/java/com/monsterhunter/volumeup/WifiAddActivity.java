package com.monsterhunter.volumeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wyzbs on 16-01-19.
 */
public class WifiAddActivity extends AppCompatActivity implements WifiSelectListAdapter.ViewHolder.IMyViewHolderClicks{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<String> m_wifiList = new ArrayList<>();
    private Set<String> m_wifiSet = new HashSet<>();
    private WifiManager m_wifiManager;
    private List<ScanResult> m_wifiScanResults = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //
        m_wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!m_wifiManager.isWifiEnabled()) {
            m_wifiManager.setWifiEnabled(true);
        }

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                m_wifiScanResults = m_wifiManager.getScanResults();
                Log.d("ABC", "GOT FIIIIII asdf asdfasd fasdf asdf asdf asdf asdf a");
                GetAvailableWifi();
                mAdapter.notifyDataSetChanged();
                unregisterReceiver(this);
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));


        m_wifiManager.startScan();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(itemDecoration);

        // specify an adapter (see also next example)
        GetAvailableWifi();
        mAdapter = new WifiSelectListAdapter(m_wifiList, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void GetAvailableWifi() {
        //while (m_wifiScanResults == null) {}
        for (ScanResult wifi : m_wifiScanResults) {
            if (!wifi.SSID.trim().isEmpty()) {
                m_wifiSet.add(wifi.SSID);
            }
        }
        for (String wifiSSID : m_wifiSet) {
            m_wifiList.add(wifiSSID);
        }
    }

    @Override
    public void onItemClick(Context context, int itemPosition) {
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        String selectedWifiSSID = m_wifiList.get(itemPosition);
        preferenceHelper.AddWifi(selectedWifiSSID);
        //
        WifiStateChangeReceiver.handleWifiStateChanged(context);
        //
        setResult(RESULT_OK);
        this.finish();
    }
}
