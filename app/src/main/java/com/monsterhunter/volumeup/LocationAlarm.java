package com.monsterhunter.volumeup;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

/**
 * Created by wyzbs on 16-01-08.
 */
public class LocationAlarm {
    static private AlarmManager alarmMgr;
    static private PendingIntent alarmIntent;

    static void turnOnAlarm(Context context) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, LocationUpdateService.class);
        alarmIntent = PendingIntent.getService(context, 0, intent, 0);
        /*
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);*/
        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        10 * 1000, alarmIntent);
        Log.d("ABC", "Alarm set");
    }

    static void turnOffAlarm() {
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }

    static void turnOffRequest(int fireInMinutes, Context context) {
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, LocationRequestHandler.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);
    }
}
