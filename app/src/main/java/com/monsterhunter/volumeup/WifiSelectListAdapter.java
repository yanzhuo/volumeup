package com.monsterhunter.volumeup;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by wyzbs on 16-01-20.
 */
public class WifiSelectListAdapter extends RecyclerView.Adapter<WifiSelectListAdapter.ViewHolder> {
    private List<String> m_data;
    private ViewHolder.IMyViewHolderClicks m_viewHolderListener;
    public WifiSelectListAdapter(List<String> wifis, ViewHolder.IMyViewHolderClicks listener) {
        m_data = wifis;
        m_viewHolderListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_item_available_wifi, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v, m_viewHolderListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String wifi = m_data.get(position);
        holder.m_wifiName.setText(wifi);
//
//        if (wifi.isSelected) {
//            holder.m_wifiName.setTextColor(Color.RED);
//        } else {
//            holder.m_wifiName.setTextColor(Color.BLACK);
//        }
    }

    @Override
    public int getItemCount() {
        return m_data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView m_wifiName;
        public IMyViewHolderClicks mListener;
        public ViewHolder(View itemView, IMyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            m_wifiName = (TextView)itemView.findViewById(R.id.wifi_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            mListener.onItemClick(v.getContext(), pos);
        }

        public interface IMyViewHolderClicks {
            void onItemClick(Context context, int itemPosition);
        }
    }



}
