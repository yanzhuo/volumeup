package com.monsterhunter.volumeup;

/**
 * Created by wyzbs on 16-01-20.
 */
public class WifiConfig {
    public String ssid;
    public String bssid;
    public boolean isSelected;

    WifiConfig(String ssid, String bssid, boolean isSelected) {
        this.ssid = ssid;
        this.bssid = bssid;
        this.isSelected = isSelected;
    }
}
