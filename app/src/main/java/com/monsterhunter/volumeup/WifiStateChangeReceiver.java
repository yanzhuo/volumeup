package com.monsterhunter.volumeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

/**
 * Created by wyzbs on 15-12-15.
 */
public class WifiStateChangeReceiver extends BroadcastReceiver {
    public static int WIFI_CONNECTED = 1;
    public static int WIFI_DISCONNECTED = 0;
    private static final String TAG = WifiStateChangeReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        handleWifiStateChanged(context, (NetworkInfo)intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO));
    }

    public static void handleWifiStateChanged(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
        handleWifiStateChanged(context, networkInfo);
    }

    private static void handleWifiStateChanged(Context context, NetworkInfo networkInfo) {
        if (networkInfo == null || networkInfo.getType() != ConnectivityManager.TYPE_WIFI) {
            return;
        }
        VolumeHandler volumeHandler = new VolumeHandler(context);
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);

        int lastWifiState = preferenceHelper.GetLastOnOffState();
        //
        Log.d(TAG, networkInfo.getDetailedState().name());
        Log.d(TAG, networkInfo.getExtraInfo());
        String reason = networkInfo.getReason();
        if (reason != null) {
            Log.d(TAG, reason);
        }
        Log.d(TAG, networkInfo.getSubtypeName());
        Log.d(TAG, networkInfo.getTypeName());
        Log.d(TAG, networkInfo.getState().name());
        //
        if(networkInfo.isConnected()) {
            if (lastWifiState != WIFI_CONNECTED) {
                Log.d("Wifi change", "wifi connected");
                //
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ssid = wifiInfo.getSSID().replace("\"", "");
                int wifiConfig = preferenceHelper.GetWifiConfig(ssid);

                switch (wifiConfig) {
                    case 0:
                        break;
                    case 1:
                        double maxWifiVolumeLevel = context.getResources().getInteger(R.integer.wifi_volumn_total_levels);
                        volumeHandler.VolumeUp(preferenceHelper.GetMaxVolume()/ maxWifiVolumeLevel);
                        break;
                    case 2:
                        volumeHandler.VolumeDown(0);
                        break;
                    default:
                        break;
                }
                /* turn on / off location alarm based on wifi connection */
                LocationRequestHandler locationRequest = new LocationRequestHandler(context);
                locationRequest.cancelRequest();
                preferenceHelper.SetLastOnOffState(WIFI_CONNECTED);
            }
        } else if (!networkInfo.isConnectedOrConnecting() && lastWifiState != WIFI_DISCONNECTED) {
            Log.d("Wifi change", "wifi disconnected");
            Logger.Append("Wifi Disconnected", true, context);
            //
            boolean turnDownVolume = preferenceHelper.getVolumeDownOnWifiDisconnect();
            if (turnDownVolume) {
                volumeHandler.VolumeDown(0);
            }
            /* turn on / off location alarm based on wifi connection */
            LocationRequestHandler locationRequest = new LocationRequestHandler(context);
            locationRequest.requestUpdate();
            preferenceHelper.SetLastOnOffState(WIFI_DISCONNECTED);
            preferenceHelper.SetIsWifiJustOff(true);
            //audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        }
    }
}
