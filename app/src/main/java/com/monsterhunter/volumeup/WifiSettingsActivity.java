package com.monsterhunter.volumeup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.monsterhunter.volumeup.choicemode.ChoiceCapableAdapter;
import com.monsterhunter.volumeup.choicemode.IChoiceMode;
import com.monsterhunter.volumeup.choicemode.MultiChoiceMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyzbs on 16-01-18.
 */
public class WifiSettingsActivity extends AppCompatActivity implements IChoiceMode.IOnCheckModeChangedListener, ActionMode.Callback {
    private RecyclerView mRecyclerView;
    private ChoiceCapableAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionMode mActionMode;
    private List<WifiListItem> mSelectedWifis;
    private FloatingActionButton mFab;
    private static int WIFI_ADD_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        IChoiceMode choiceModeHandler = new MultiChoiceMode();
        choiceModeHandler.setOnCheckModeChangedListener(this);

        mSelectedWifis = new ArrayList<>();
        getWifis();
        mAdapter = new ListViewAdapterWifiConfig(mSelectedWifis, this, choiceModeHandler);
        mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(itemDecoration);


        mFab = (FloatingActionButton) findViewById(R.id.fab);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(getApplicationContext(), WifiAddActivity.class);
                startActivityForResult(intent, WIFI_ADD_REQUEST);
                //startActivity(intent);
            }
        });
    }

    private void getWifis() {
        mSelectedWifis.clear();
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.GetWifiItems(mSelectedWifis);
    }

    @Override
    public void OnCheckModeChanged(boolean isOnCheckedMode) {
        if (isOnCheckedMode) {
            mActionMode = startSupportActionMode(this);
            mFab.animate().translationXBy(300);
        } else {
            if (mActionMode != null) {
                mActionMode.finish();
            }
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        getMenuInflater().inflate(R.menu.menu_context_mode, menu);
        mode.setTitle("DELETE");
        return(true);
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item_remove:
                mAdapter.deleteCheckedItems();
                mActionMode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mFab.animate().translationXBy(-300);
        mAdapter.clearCheckedState();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == WIFI_ADD_REQUEST) {
            if (resultCode == RESULT_OK) {
                getWifis();
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
