package com.monsterhunter.volumeup;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.google.android.gms.location.LocationResult;
import com.monsterhunter.volumeup.location.LocationConfig;

import java.util.List;

/**
 * Created by wyzbs on 16-01-09.
 */
public class LocationIntentService extends IntentService {

    private static String TAG = LocationIntentService.class.getName();
    private static int WIFI_TOGGLE_DISTANCE = 40;
    private static int[] LocationCheckInterval = {2,4,8,12,15,20,30};
    private static int[] LocationLevel = {60, 100, 600, 2000, 4000, 10000, 20000};

    public LocationIntentService() {
        super("Location Intent Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "received location update");
        //PreferenceHelper prefHelper = new PreferenceHelper(getApplicationContext());

//        if (prefHelper.getIsLocationRequestJustSent()) {
//            prefHelper.setIsLocationRequestJustSent(false);
//            return;
//        }

        if (LocationResult.hasResult(intent)) {
            LocationResult locationResult = LocationResult.extractResult(intent);
            Location updatedLocation = locationResult.getLastLocation();

            if (updatedLocation != null) {
                handleNewLocation(updatedLocation);
            }
        }
    }

    private void handleNewLocation(Location myLocation) {
        //Log.d(TAG, "location is " + myLocation.getLatitude() + " " + myLocation.getLongitude());
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        List<LocationConfig> locations = preferenceHelper.GetAllLocationConfigs();

        if (locations.size() != 0) {
            float distToMyLocations = Integer.MAX_VALUE;
            for (LocationConfig l : locations) {
                Location hotspot = new Location("new");
                hotspot.setLatitude(l.coordinate.latitude);
                hotspot.setLongitude(l.coordinate.longitude);
                distToMyLocations = Math.min(myLocation.distanceTo(hotspot), distToMyLocations);
            }
            Log.d(TAG, "distance: " + distToMyLocations);
            processDistance(distToMyLocations);
        }
        this.stopSelf();
    }

    private void processDistance(float distance) {
        // determin time interval
        int distIdx = 0;
        for(; distIdx < LocationLevel.length; ++distIdx) {
            if (distance < LocationLevel[distIdx]) {
                break;
            }
        }
        if (distIdx == 0) { // should connect
            toggleWifi(true);
            updateLocationRequestInterval(5);
        } else {
            toggleWifi(false);

            updateLocationRequestInterval(LocationCheckInterval[distIdx - 1]);
            Log.d(TAG, "index: " + distIdx + " interval: " + LocationCheckInterval[distIdx-1]);
            Logger.Append("dist:" + distance + " interval: " + LocationCheckInterval[distIdx-1], true, super.getApplicationContext());
        }
    }

    private void updateLocationRequestInterval(int interval) {
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        int curInterval = preferenceHelper.getLocationUpdateInterval();
        if (curInterval != interval) {
            preferenceHelper.setLocationUpdateInterval(interval);
            LocationRequestHandler locationRequestHandler = new LocationRequestHandler(this);
            locationRequestHandler.requestUpdateNewInterval(interval);
        }
    }

    private void toggleWifi(boolean isEnabled) {
        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() != isEnabled) {
            wifiManager.setWifiEnabled(isEnabled);
        }
    }
}
