package com.monsterhunter.volumeup;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.preference.Preference;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;


public class SeekBarPreference extends Preference implements OnSeekBarChangeListener {
    private SeekBar mSeekBar;
    private int mMaxProgress;
    private int mProgress;


    private static final int DEFAULT_MAX_VALUE = 100;
    private String mTitle = null;

    public SeekBarPreference(Context context) {
        this(context, null, 0);
    }

    public SeekBarPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayoutResource(R.layout.preference_seekbar);
        setValuesFromXml(attrs, context);
    }

    private void setValuesFromXml(@Nullable AttributeSet attrs, Context context) {

        if (attrs == null) {
            mMaxProgress = DEFAULT_MAX_VALUE;

        } else {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekBarPreference);
            try {
                mMaxProgress = a.getInt(R.styleable.SeekBarPreference__maxValue, DEFAULT_MAX_VALUE);
                mTitle = a.getString(R.styleable.SeekBarPreference__title);
            } finally {
                a.recycle();
            }
        }
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        mSeekBar = (SeekBar) view.findViewById(R.id.seekbar);
        mSeekBar.setMax(mMaxProgress);
        mSeekBar.setProgress(mProgress);
        mSeekBar.setOnSeekBarChangeListener(this);

        if (mTitle != null && !mTitle.isEmpty()) {
            TextView tvTitle = (TextView)view.findViewById(R.id.tv_seekbar_title);
            if (tvTitle != null) {
                tvTitle.setText(mTitle);
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mProgress = progress;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // not used
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        persist();
        mSeekBar.invalidate();
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        mProgress = restoreValue ? getPersistedInt(mProgress) : (Integer) defaultValue;
    }

    public void persist() {
        if (shouldPersist()) {
            persistInt(mProgress);
            notifyChanged();
        }
    }

    public void setProgress(int progress) {
        mProgress = progress;
        persist();
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }

    public void setMax(int max) {
        mMaxProgress = max;
        if (mSeekBar != null) {
            mSeekBar.setMax(max);
        }
    }
}
