package com.monsterhunter.volumeup;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.jar.*;

/**
 * Created by wyzbs on 2016-12-17.
 */

public class Logger {

    private static final String LOG_FILE_NAME = "VolumeUp_Log.txt";



    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    static void Append(String message, boolean printTrace, Context context) {

        if (!isExternalStorageWritable()) {
            return;
        }

        final String packageName = context.getPackageName();

        File logDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), packageName);

        if (!logDir.exists())
        {
            if (!logDir.mkdirs()) {
                return;
            }

        }

        File logFile = new File (logDir, LOG_FILE_NAME);
        if (!logFile.exists()) {
            try {
                if (!logFile.createNewFile()) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        try
        {
            // calling method
            if (printTrace) {
                StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
                message = stackTraceElements[5].getMethodName() + ": " + message;
            }

            // time
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            String formattedDate = df.format(c.getTime());
            message = "[" + formattedDate + "]"+ message;

            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(message);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
