package com.monsterhunter.volumeup;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by wyzbs on 16-01-06.
 */
public class LocationUpdateService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private LocationManager m_locationManager;
    private PowerManager.WakeLock m_wakeLock;
    private GoogleApiClient m_GoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final String TAG = LocationUpdateService.class.getSimpleName();
    private LocationRequest m_LocationRequest;
    private static int WIFI_TOGGLE_DISTATNCE = 20;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 23
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(m_GoogleApiClient);

        if (location == null || (System.currentTimeMillis() - location.getTime())/(1000*60) > 15) {
            LocationServices.FusedLocationApi.requestLocationUpdates(m_GoogleApiClient, m_LocationRequest, this);
            Log.d(TAG,"location service get last failed, requesting");
        } else {
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Location service connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        this.stopSelf();
    }

    private enum State {
        IDLE, WORKING, REQUESTING
    }

    private static State state;

    static {
        state = State.IDLE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.m_wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LocationUpdateService");

        // google play
        m_GoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        m_LocationRequest = new LocationRequest();
        m_LocationRequest.setInterval(10000);
        m_LocationRequest.setFastestInterval(10000);
        m_LocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        Log.d(TAG, "creating service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (state == State.IDLE) {
            state = State.WORKING;
            this.m_wakeLock.acquire();
            m_GoogleApiClient.connect();
        }

        Log.d(TAG, "starting service");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.m_wakeLock.isHeld()) {
            this.m_wakeLock.release();
        }

        if (state == State.REQUESTING) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        m_GoogleApiClient, this);
            } catch(Exception e){

            }
        }
        state = State.IDLE;

        if (m_GoogleApiClient.isConnected()) {
            m_GoogleApiClient.disconnect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void handleNewLocation(Location myLocation) {
        Log.d(TAG, "location is " + myLocation.getLatitude() + " " + myLocation.getLongitude());
        Location homeLocation = new Location("my home");
        homeLocation.setLongitude(-87.618708);
        homeLocation.setLatitude(41.889900);
        float distanceToHome = myLocation.distanceTo(homeLocation);
        Log.d(TAG, "I'm " + distanceToHome + "m from home");
        if (distanceToHome < WIFI_TOGGLE_DISTATNCE) {
            // turn on wifi
            toggleWifi(true);
        } else {
            toggleWifi(false);
        }
        this.stopSelf();
    }

    private void toggleWifi(boolean isEnabled) {
        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() != isEnabled) {
            wifiManager.setWifiEnabled(isEnabled);
        }
    }
}
