package com.monsterhunter.volumeup;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

/**
 * Created by wyzbs on 16-01-18.
 */
public class MainSettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.root_preference);

        // seekbar
        SeekBarPreference maxVolSeekBar = (SeekBarPreference) findPreference(getString(R.string.prefs_key_wifi_volume));
        maxVolSeekBar.setMax(getResources().getInteger(R.integer.wifi_volumn_total_levels));

        // wifi swtich
        Preference wifiSwitchPref = findPreference(getString(R.string.prefs_key_wifi_swtich));
        wifiSwitchPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean isEnabled = (boolean) newValue;
                PackageManager packageManager = getActivity().getPackageManager();
                ComponentName component = new ComponentName(getActivity(), WifiStateChangeReceiver.class);
                int status = packageManager.getComponentEnabledSetting(component);

                if (isEnabled) {
                    if (status != PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
                        packageManager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    }
//                    VolumeHandler volumeHandler = new VolumeHandler(getActivity());
//                    volumeHandler.HandleWifiChange(networkInfo);
                    WifiStateChangeReceiver.handleWifiStateChanged(getActivity());
                } else {
                    if (status != PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
                        packageManager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                        PreferenceHelper preferenceHelper = new PreferenceHelper(getActivity());
                        preferenceHelper.SetLastOnOffState(WifiStateChangeReceiver.WIFI_DISCONNECTED);
                    }
                }
                return true;
            }
        });

        // location switch
        Preference locationSwitchPref = findPreference(getString(R.string.prefs_key_location_switch));
        locationSwitchPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean isEnabled = (boolean) newValue;
                preference.getSharedPreferences()
                        .edit()
                        .putBoolean(preference.getKey(), isEnabled)
                        .commit();
                //
                LocationRequestHandler locationRequest = new LocationRequestHandler(getActivity());
                if (isEnabled) {
                    locationRequest.requestUpdate();
                } else {
                    locationRequest.cancelRequest();
                }
                return true;
            }
        });
    }

}
