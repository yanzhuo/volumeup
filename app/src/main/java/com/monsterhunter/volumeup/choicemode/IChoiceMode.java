package com.monsterhunter.volumeup.choicemode;

import android.os.Bundle;

import java.util.List;

/**
 * Created by wyzbs on 16-01-26.
 */
public interface IChoiceMode {
    void setChecked(int position, boolean isChecked);
    boolean isChecked(int position);
    void onSaveInstanceState(Bundle state);
    void onRestoreInstanceState(Bundle state);
    int getCheckedCount();
    void clearChecks();
    boolean toggleChecks(int position);
    boolean isInCheckMode();
    void setIsCheckMode(boolean isInCheckMode);
    List<Integer> getCheckedPositions();
    void setOnCheckModeChangedListener(IOnCheckModeChangedListener listener);

    interface IOnCheckModeChangedListener {
        void OnCheckModeChanged(boolean isOnCheckedMode);
    }
}
