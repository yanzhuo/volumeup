package com.monsterhunter.volumeup.choicemode;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by wyzbs on 16-01-31.
 */
public class ParcelableIntegerSet implements Parcelable {
    
    private Set<Integer> mData = new HashSet<>();

    public ParcelableIntegerSet() {}

    protected ParcelableIntegerSet(Parcel parcel) {
        int size = parcel.readInt();

        for(int i = 0; i < size; i++) {
            mData.add(parcel.readInt());
        }
    }

    public static final Creator<ParcelableIntegerSet> CREATOR = new Creator<ParcelableIntegerSet>() {
        @Override
        public ParcelableIntegerSet createFromParcel(Parcel in) {
            return new ParcelableIntegerSet(in);
        }

        @Override
        public ParcelableIntegerSet[] newArray(int size) {
            return new ParcelableIntegerSet[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mData.size());
        for(Integer d : mData) {
            dest.writeInt(d);
        }
    }

    public int size() {
        return mData.size();
    }

    public boolean add(int data) {
        return mData.add(data);
    }

    public boolean remove(int data) {
        return mData.remove(data);
    }

    public boolean contains(int data) {
        return mData.contains(data);
    }

    public Collection<Integer> getAll() {
        return mData;
    }

    public void clear() {
        mData.clear();
    }
}
