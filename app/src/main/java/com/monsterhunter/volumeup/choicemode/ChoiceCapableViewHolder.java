package com.monsterhunter.volumeup.choicemode;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by wyzbs on 16-01-31.
 */
abstract public class ChoiceCapableViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
    protected IChoiceMode mChoiceModeHandler;

    public ChoiceCapableViewHolder(View v, IChoiceMode choiceMode) {
        super(v);
        this.mChoiceModeHandler = choiceMode;
        v.setOnClickListener(this);
        v.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mChoiceModeHandler.isInCheckMode()) {
            boolean isChecked = mChoiceModeHandler.toggleChecks(getAdapterPosition());
            v.setSelected(isChecked);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        boolean isChecked = mChoiceModeHandler.toggleChecks(getAdapterPosition());
        v.setSelected(isChecked);
        return true;
    }

}
