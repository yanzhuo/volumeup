package com.monsterhunter.volumeup.choicemode;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyzbs on 16-01-26.
 */
public class MultiChoiceMode implements IChoiceMode {
    private static final String STATE_CHECK_STATES="checkedPositions";
    private ParcelableIntegerSet mCheckedPositions;
    private boolean mIsInCheckMode;
    private IOnCheckModeChangedListener mOnCheckedModeChangedListener;

    public MultiChoiceMode() {
        mIsInCheckMode = false;
        mCheckedPositions = new ParcelableIntegerSet();
    }

    @Override
    public void setChecked(int position, boolean isChecked) {
        if (isChecked) {
            mCheckedPositions.add(position);
            updateCheckModeAndNotify();
        }
        else {
            mCheckedPositions.remove(position);
            updateCheckModeAndNotify();
        }
    }

    @Override
    public boolean isChecked(int position) {
        return(mCheckedPositions.contains(position));
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putParcelable(STATE_CHECK_STATES, mCheckedPositions);
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        mCheckedPositions =state.getParcelable(STATE_CHECK_STATES);
    }

    @Override
    public int getCheckedCount() {
        return(mCheckedPositions.size());
    }

    @Override
    public void clearChecks() {
        mCheckedPositions.clear();
        mIsInCheckMode = false;
    }

    @Override
    public boolean toggleChecks(int position) {
        if (mCheckedPositions.contains(position)) {
            mCheckedPositions.remove(position);
            updateCheckModeAndNotify();
            return false;
        } else {
            mCheckedPositions.add(position);
            updateCheckModeAndNotify();
            return true;
        }
    }

    @Override
    public boolean isInCheckMode() {
        return mIsInCheckMode;
    }

    @Override
    public void setIsCheckMode(boolean isInCheckMode) {
        if (mIsInCheckMode != isInCheckMode) {
            mCheckedPositions.clear();
        }
        mIsInCheckMode = isInCheckMode;
    }

    @Override
    public List<Integer> getCheckedPositions() {
        List<Integer> ret = new ArrayList<>();
        ret.addAll(mCheckedPositions.getAll());
        return ret;
    }

    @Override
    public void setOnCheckModeChangedListener(IOnCheckModeChangedListener listener) {
        mOnCheckedModeChangedListener = listener;
    }

    private void updateCheckModeAndNotify() {
        boolean isOnCheckMode = mCheckedPositions.size() > 0;

        if (mIsInCheckMode != isOnCheckMode) {
            mIsInCheckMode = isOnCheckMode;
            if (mOnCheckedModeChangedListener != null) {
                mOnCheckedModeChangedListener.OnCheckModeChanged(isOnCheckMode);
            }
        }
    }
}
