package com.monsterhunter.volumeup.choicemode;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

/**
 * Created by wyzbs on 16-01-26.
 */
abstract public class
        ChoiceCapableAdapter<T extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<T> {
    protected final IChoiceMode choiceMode;

    public ChoiceCapableAdapter(IChoiceMode choiceMode) {
        super();
        this.choiceMode = choiceMode;
    }

    public void onSaveInstanceState(Bundle state) {
        choiceMode.onSaveInstanceState(state);
    }

    public void onRestoreInstanceState(Bundle state) {
        choiceMode.onRestoreInstanceState(state);
    }

    abstract public void deleteCheckedItems();
    abstract public void clearCheckedState();
}
